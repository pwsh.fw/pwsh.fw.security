[CmdletBinding()]Param(
	[switch]$SkipNuGet,
	[switch]$Force
)

. $PSScriptRoot/00_header.ps1 -SkipNuGet:$SkipNuGet -Force:$Force

$BASENAME = Split-Path -Path $PSCommandPath -Leaf
Write-Host -ForegroundColor Blue ">> $BASENAME"

# add local repo
Get-PSRepository
$LocalPSRepo = Get-PSRepository -Name LocalPSRepo
if (!($LocalPSRepo)) {
	# $LocalPSRepo = Register-PSRepository -Name LocalPSRepo -SourceLocation $env:WES_NETWORKSHARE -ScriptSourceLocation $env:WES_NETWORKSHARE -InstallationPolicy Trusted
	Register-PSRepository -Name LocalPSRepo -SourceLocation $env:NUGET_LOCALURL -PublishLocation $env:NUGET_LOCALURL -PackageManagementProvider nuget -InstallationPolicy Trusted
	$LocalPSRepo = Get-PSRepository -Name LocalPSRepo
}

Update-ModuleManifest -Path $ROOTDIR/$($project.Name)/$($project.Name).psd1 -ModuleVersion $($project.Version) -PreRelease $($project.PreRelease)

if (($env:CI_COMMIT_BRANCH -eq "master") -or (![string]::IsNullOrEmpty($env:CI_COMMIT_TAG))) {
	if ($PSGallery)		{ Publish-Module -Path $ROOTDIR/$($project.Name) -Repository PSGallery   -NuGetApiKey $env:NUGET_API_KEY_2020 -SkipAutomaticTags -Verbose -Force }
	if ($LocalPSRepo)	{ Publish-Module -Path $ROOTDIR/$($project.Name) -Repository LocalPSRepo -NuGetApiKey $env:NUGET_LOCALAPIKEY  -SkipAutomaticTags -Verbose -Force }
} else {
	if ($PSGallery)		{ Publish-Module -Name $ROOTDIR/$($project.Name) -Repository PSGallery   -NuGetApiKey $env:NUGET_API_KEY_2020 -SkipAutomaticTags -Verbose -Force -AllowPrerelease }
	if ($LocalPSRepo)	{ Publish-Module -Name $ROOTDIR/$($project.Name) -Repository LocalPSRepo -NuGetApiKey $env:NUGET_LOCALAPIKEY  -SkipAutomaticTags -Verbose -Force -AllowPrerelease }
}

Write-Host -ForegroundColor Blue "<< $BASENAME"
