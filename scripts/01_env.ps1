#requires -PSEdition Core

[CmdletBinding()]Param(
	[switch]$SkipNuGet,
	[switch]$Force
)

. $PSScriptRoot/00_header.ps1 -SkipNuGet:$SkipNuGet -Force:$Force

$BASENAME = Split-Path -Path $PSCommandPath -Leaf
Write-Host -ForegroundColor Blue ">> $BASENAME"

Get-Location
Get-ChildItem Env:\ | Where-Object { $_.Name -like "CI_*" -or $_.Name -like "GITLAB_*" } | Format-Table Name, Value

Write-Host -ForegroundColor Blue "<< $BASENAME"
