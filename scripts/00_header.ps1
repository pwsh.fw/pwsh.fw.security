[CmdletBinding()]Param(
	[switch]$SkipNuGet,
	[switch]$Force
)

$ErrorActionPreference = "Stop"

$ROOTDIR = (Resolve-Path $PSScriptRoot/../).Path
Write-Host -ForegroundColor Blue ">> header"

# clean to avoid doubles
Remove-Module PwSh.Fw.*
if (-not ($SkipNuGet)) {
	if (!(Get-PackageProvider -Name NuGet)) { Install-PackageProvider -Name NuGet -Force:$Force -Confirm:$false }
	$PSGallery = Get-PSRepository -Name PSGallery
	if (!($PSGallery)) {
		$PSGallery = Register-PSRepository -Default -InstallationPolicy Trusted
	} else {
		Set-PSRepository -Name PSGallery -InstallationPolicy Trusted
	}
	Install-Module PwSh.Fw.Core -Repository PSGallery -ErrorAction stop -Force:$Force -AllowPrerelease -AllowClobber
	Install-Module PwSh.Fw.BuildHelpers -Repository PSGallery -ErrorAction stop -Force:$Force -AllowPrerelease -AllowClobber
}
Import-Module PwSh.Fw.Core -DisableNameChecking -Force:$Force
Import-Module PwSh.Fw.BuildHelpers -DisableNameChecking -Force:$Force -ErrorAction stop

$Global:QUIET = $false
$Global:VERBOSE = $true
$Global:DEBUG = $true
$Global:DEVEL = $true
$Global:TRACE = $true

# Get-Module -Name PwSh.Fw.* -ListAvailable
$PSVersionTable | Format-Table Name, Value -AutoSize
$project = Get-Project -Path $ROOTDIR
$project | Format-Table Name, Value -AutoSize
# $project

Write-Host -ForegroundColor Blue "<< header"
