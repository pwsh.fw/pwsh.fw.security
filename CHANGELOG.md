# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.2.0] - 2022-09-28

### Added

-	publish to local repo

### Fixed

-	version number

## [1.1.1] - 2022-09-28

### Fixed

-	`Save-Credentials` support username with `DOMAIN\Username` format

## [1.1.0] - 2021-06-01

### Added

-	`Load-Credentials` support username with `DOMAIN\Username` format

## [1.0.1] - 2021-05-04

### Fixed

-	Save credentials with domain

## [1.0.0] - 2021-02-18

### Added

Save / Load / Show credentials to/from local encoded files.
Password can only be loaded by the user and on the machine where they were created.

-	You cannot move files to another computer, you won't be able to decipher it.
-	You cannot give files to another user, (s)he will not be able to decipher it too.
