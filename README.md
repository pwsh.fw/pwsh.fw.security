[![Project Status: Active – The project has reached a stable, usable state and is being actively developed.](https://www.repostatus.org/badges/latest/active.svg)](https://www.repostatus.org/#active)
[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0)

[![Pipeline status - master](https://gitlab.com/pwsh.fw/pwsh.fw.security/badges/master/pipeline.svg)](https://gitlab.com/pwsh.fw/pwsh.fw.security/pipelines)
[![Pipeline status - develop](https://gitlab.com/pwsh.fw/pwsh.fw.security/badges/develop/pipeline.svg)](https://gitlab.com/pwsh.fw/pwsh.fw.security/pipelines)

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/b95ad686a8d04676b39f7fd036d36083)](https://www.codacy.com/gl/pwsh.fw/pwsh.fw.security?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=pwsh.fw/pwsh.fw.security&amp;utm_campaign=Badge_Grade)

[![PowerShell Gallery - Version](https://img.shields.io/powershellgallery/v/pwsh.fw.security)](https://www.powershellgallery.com/packages/pwsh.fw.security)
[![PowerShell Gallery](https://img.shields.io/powershellgallery/dt/pwsh.fw.security)](https://www.powershellgallery.com/packages/pwsh.fw.security)
[![Powershell Platform](https://img.shields.io/powershellgallery/p/pwsh.fw.security)](https://www.powershellgallery.com/packages/pwsh.fw.security)

<img align="left" width="48" height="48" src="images/favicon.png">

# pwsh.fw.security

PwSh Framework Security module.

## Content

`pwsh.fw.security` contains helper function to interact with credentials

It can Save / Load / Show credentials to/from local encoded files.
Password can only be loaded by the user and on the machine where they were created.

-	You cannot move files to another computer, you won't be able to decipher it.
-	You cannot give files to another user, (s)he will not be able to decipher it too.

## How it works

