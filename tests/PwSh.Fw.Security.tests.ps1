# Invoke code coverage
# Invoke-Pester -Script ./PwSh.Fw.Archive.tests.ps1 -CodeCoverage ../PwSh.Fw.Archive/PwSh.Fw.Archive.psm1

$BASENAME = Split-Path -Path $PSCommandPath -Leaf
$ModuleName = $BASENAME -replace ".tests.ps1"

# load header
. $PSScriptRoot/header.inc.ps1

Import-Module -FullyQualifiedName $ROOTDIR/$ModuleName/$ModuleName.psm1 -Force -ErrorAction stop

Describe "$ModuleName" {

	# clean things
	Remove-Item "$TEMP/*.pwd" -Force -ErrorAction SilentlyContinue

	Context "Credentials user / pass" {

		It "Save credentials to a file #1" {
			$SecurePassword = "testP@sswørd" | ConvertTo-SecureString -AsPlainText
			$rc = Save-Credentials -Username "testUser" -Password $SecurePassword -Path $TEMP
			$rc | Should -BeTrue
			Test-Path "$TEMP/testUser.pwd" -PathType Leaf | should -BeTrue
		}

		It "Load credentials from a file #1" {
			$creds = Load-Credentials -Username "testUser" -Path $TEMP
			$creds | Should -Not -BeNullOrEmpty
		}

		It "Show credentials from a file #1" {
			$user, $pass = Show-Credentials -Username "testUser" -Path $TEMP
			$user | should -BeExactly "testUser"
			$pass | should -BeExactly "testP@sswørd"
		}

	}

	# clean things
	Remove-Item "$TEMP/*.pwd" -Force -ErrorAction SilentlyContinue

}